package de.ubt.ai4.petter.recpro.lib.rating.ratingexecution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.ubt.ai4.petter.recpro.lib.rating.ratingexecution", "de.ubt.ai4.petter.recpro.lib.rating.rating", "de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence", "de.ubt.ai4.petter.recpro.lib.ontology"})
public class RatingExecutionApplication {

	public static void main(String[] args) {
		SpringApplication.run(RatingExecutionApplication.class, args);
	}

}
