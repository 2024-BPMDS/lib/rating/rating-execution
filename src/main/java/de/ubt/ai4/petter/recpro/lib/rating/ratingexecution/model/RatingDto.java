package de.ubt.ai4.petter.recpro.lib.rating.ratingexecution.model;

import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.Rating;
import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model.RatingInstance;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RatingDto {

    private Rating rating;
    private RatingInstance instance;
}
